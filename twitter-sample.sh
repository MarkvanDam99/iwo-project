#!/bin/bash

echo Total amount of tweets on 01/03/2017 at 12:00:
#get the tweets and their info | get only the tweets | count the amount of tweets
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l 

echo Total amount of unique tweets on 01/03/2017 at 12:00:
#get the tweets and their info | get only the tweets | leaves only the unique tweets | count the amount of tweets
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | wc -l

echo Total amount of retweets on 01/03/2017 at 12:00:
#get the tweets and their info | get only the tweets | leaves only the unique tweets | get the tweets that start with RT | count the amount of tweets
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep ^RT | wc -l

echo First 20 unique tweets in the sample that are not retweets on 01/03/2017 at 12:00:
#get the tweets and their info | get only the tweets | leaves only the unique tweets | get the tweets that do'nt start with RT | output the first 20 tweets
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep -v ^RT | head -20

