import sys

female=[name.strip() for name in open("names/nl-female.txt").readlines()]
male=[name.strip() for name in open("names/nl-male.txt").readlines()]
beter_dan_f = 0
beter_dan_m = 0
beter_als_f = 0
beter_als_m = 0

for line in sys.stdin:
    name = line.strip().split("\t")[0].lower() # assumes name is first in tab-separated input
    f=0
    m=0
    for n in female:
        if n in name:
            f+=1
    for n in male:
        if n in name:
            m+=1

    if f > m:
        line= "female\t{}".format(line.strip())
	list_f=line.split('	')
	if not list_f[2].startswith('RT') and "beter dan" in list_f[2]:
            beter_dan_f+=1
	elif not list_f[2].startswith('RT') and "beter als" in list_f[2]:
            beter_als_f+=1
          
    elif m > f:
        line= "male\t{}".format(line.strip())
	list_m=line.split('	')
	if not list_m[2].startswith('RT') and "beter dan" in list_m[2]:
            beter_dan_m+=1
	elif not list_m[2].startswith('RT') and "beter als" in list_m[2]:
            beter_als_m+=1

    else:
        pass 

print("goed vrouwen:{}".format(beter_dan_f))
print("goed mannen:{}".format(beter_dan_m))
print("fout vrouwen:{}".format(beter_als_f))
print("fout mannen:{}".format(beter_als_m))